//
//  GeoLocationManager.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 21/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import Foundation
import CoreLocation


class GeoLocationManager
{
    
    static func getAddressFromLocation( longitude:Double, latitude: Double, callBack: @escaping ( _ address: String )-> Void )
    {
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: latitude, longitude: longitude)
        geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            
            // Place details
            var placeMark: CLPlacemark!
            placeMark = placemarks?[0]
            
            // Address dictionary
            print(placeMark.addressDictionary as Any)
            var addresses = ""
            // Location name
            if let locationName = placeMark.addressDictionary!["Name"] as? NSString {
                print(locationName)
                addresses += "\(locationName) "
            }
            // Street address
            if let street = placeMark.addressDictionary!["Thoroughfare"] as? NSString {
                print(street)
                addresses += "\(street), "
            }
            // City
            if let city = placeMark.addressDictionary!["City"] as? NSString {
                print(city)
                addresses += "\(city), "
            }
            // Zip code
            if let zip = placeMark.addressDictionary!["ZIP"] as? NSString {
                print(zip)
                addresses += "\(zip) "
            }
            // Country
            if let country = placeMark.addressDictionary!["Country"] as? NSString {
                print(country)
                addresses += "\(country) "
            }
            
            callBack(addresses)
            
        })
    }
    
}

//
//  User.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 19/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

class User
{
    var authKey = ""
    var companyCode = ""
    var companyId = ""
    var companyName = ""
    var countryId = ""
    var countryName = ""
    var currency = ""
    var dateFormat = ""
    var languageId = ""
    var languageText = ""
    var baseLanguageId = ""
    var currentCompanyId = ""
    var currentCompanyLogo = ""
    var currentCompanyName = ""
    var currentConnectionString = ""
    var defaultGlobalProjectId = ""
    var departmentId = ""
    var designationId = ""
    var employeeCode = ""
    var employeeDesignationId = ""
    var employeeEmail = ""
    var employeeId = ""
    var employeeName = ""
    var encryptionHashKey = ""
    var genderId = ""
    var instanceName = ""
    var locationId = ""
    var userId = ""
    var userMasterId = ""
    var userTypeId = ""
    var valconClientId = ""
    
    init(userId: String, userMasterId: String, userTypeId: String, authKey: String, defaultGlobalProjectId: String  )
    {
        self.userId = userId
        self.userMasterId = userMasterId
        self.userTypeId = userTypeId
        self.authKey = authKey
        self.defaultGlobalProjectId = defaultGlobalProjectId
    }
    
    init( userId: String,  userMasterId: String, userTypeId: String, authKey: String, defaultGlobalProjectId: String, companyCode: String?, companyId: String?,   companyName: String?,  countryId: String?, countryName: String?, currency: String?, dateFormat: String?, languageId: String?, languageText: String?, currentCompanyId: String?,  currentCompanyLogo: String?, currentCompanyName: String?,  departmentId: String?, employeeCode: String?, employeeId: String?,employeeName: String?, encryptionHashKey: String?, instanceName: String?, locationId: String?,   valconClientId: String?   )
    {
        
        self.userId = userId
        self.userMasterId = userMasterId
        self.userTypeId = userTypeId
        self.authKey = authKey
        self.defaultGlobalProjectId = defaultGlobalProjectId
        self.companyCode = companyCode ?? ""
        self.companyId = companyId ?? ""
        self.companyName = companyName ?? ""
        self.countryId = countryId ?? ""
        self.countryName = countryName ?? ""
        self.currency = currency ?? ""
        self.dateFormat = dateFormat ?? ""
        self.languageId = languageId ?? ""
        self.languageText = languageText ?? ""
        self.currentCompanyId = companyId ?? ""
        self.currentCompanyLogo = currentCompanyLogo ?? ""
        self.currentCompanyName = currentCompanyName ?? ""
        self.departmentId = departmentId ?? ""
        self.employeeCode = employeeCode ?? ""
        self.employeeId = employeeId ?? ""
        self.employeeName = employeeName ?? ""
        self.encryptionHashKey = encryptionHashKey ?? ""
        self.instanceName = instanceName ?? ""
        self.locationId = locationId ?? ""
        self.valconClientId = valconClientId ?? ""
        
        
    }
    
    public static func createUser( userData: JSON ) -> User
    {
        var user = User(userId: userData["LoginDetail"]["UserId"].stringValue, userMasterId: userData["LoginDetail"]["UserMasterId"].stringValue, userTypeId: userData["LoginDetail"]["UserTypeId"].stringValue, authKey: userData["DefaultValue"]["AuthKey"].stringValue, defaultGlobalProjectId: userData["LoginDetail"]["DefaultGlobalProjectId"].stringValue, companyCode: userData["DefaultValue"]["CompanyCode"].stringValue, companyId: userData["DefaultValue"]["CompanyId"].stringValue, companyName: userData["DefaultValue"]["CountryName"].stringValue, countryId: userData["DefaultValue"]["CountryId"].stringValue, countryName: userData["DefaultValue"]["CountryName"].stringValue, currency: userData["DefaultValue"]["Currency"].stringValue, dateFormat: userData["DefaultValue"]["DateFormat"].stringValue, languageId: userData["DefaultValue"]["LanguageId"].stringValue, languageText: userData["DefaultValue"]["LanguageText"].stringValue, currentCompanyId: userData["LoginDetail"]["CurrentCompanyId"].stringValue, currentCompanyLogo: userData["LoginDetail"]["CurrentCompanyLogo"].stringValue, currentCompanyName: userData["LoginDetail"]["CurrentCompanyName"].stringValue, departmentId: userData["LoginDetail"]["DepartmentId"].stringValue, employeeCode: userData["LoginDetail"]["EmployeeCode"].stringValue, employeeId: userData["LoginDetail"]["EmployeeId"].stringValue, employeeName: userData["LoginDetail"]["EmployeeName"].stringValue, encryptionHashKey: userData["LoginDetail"]["EncryptionHashKey"].stringValue, instanceName: userData["LoginDetail"]["InstanceName"].stringValue, locationId: userData["LoginDetail"]["LocationId"].stringValue, valconClientId: userData["LoginDetail"]["ValconClientId"].stringValue)
        
        
        return user
    }
}

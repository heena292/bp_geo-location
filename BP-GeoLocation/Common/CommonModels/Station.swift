//
//  Station.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 21/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import Foundation
import MapKit

class Station: NSObject, MKAnnotation {
    var title: String?
    var subtitle: String?
    var latitude: Double
    var longitude:Double
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
    }
    
    init(latitude: Double, longitude: Double) {
        self.latitude = latitude
        self.longitude = longitude
    }
}

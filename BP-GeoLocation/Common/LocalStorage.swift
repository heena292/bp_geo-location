//
//  LocalStorage.swift
//  BP-Finannce
//
//  Created by Hardeep Singh on 15/07/17.
//  Copyright © 2017 Hardeep Singh. All rights reserved.
//

import Foundation
import UIKit

class LocalStorage
{
    fileprivate static var token:String = ""
    
    fileprivate static var email:String = ""
    static let defaults = UserDefaults.standard
    static let deviceId = UIDevice.current.identifierForVendor!.uuidString  as String
    
    
    internal static func get_email() -> String
    {
        
        //if LocalStorage.email == ""
        //  {
        if let username = LocalStorage.defaults.value(forKey: defaultsKeys.userKey)
        {
            LocalStorage.email = username as! String
        }
        
        
        
        
        
        //let token = defaults.valueForKey(defaultsKeys.token) as! String
        
        
        // }
        
        
        
        return LocalStorage.email
    }
    
    internal static func get_token(_ updated:Bool = false ) -> String
    {
        
        return "{751C87BB-BBA8-4151-BE7A-0E58AB8365FA}"
        //return "d6ace5b681bd5c4e3ac4817dff2c8e03"
        if LocalStorage.token == "" || updated == true {
            
            if let token = LocalStorage.defaults.value(forKey: defaultsKeys.token) as? String
            {
                if token.range( of: LocalStorage.deviceId  ) != nil{
                    
                    let newToken = token.replacingOccurrences(of: deviceId, with: "")
                    LocalStorage.token = newToken
                }
            }
            
        }
        
        
        return LocalStorage.token
    }
    
    
    internal static func update_email( _ email: String )
    {
        
        LocalStorage.defaults.setValue(email, forKey: defaultsKeys.userKey )
        
        
        
    }
    
    internal static func update_token( _ token: String )
    {
        
        LocalStorage.defaults.setValue( ((token as String) + LocalStorage.deviceId ) , forKey: defaultsKeys.token )
        LocalStorage.token = token as String
        
        
    }
    
    internal static func remove_data()
    {
       LocalStorage.defaults.setValue(  "" , forKey: defaultsKeys.token )
        LocalStorage.defaults.setValue(  "" , forKey: defaultsKeys.userKey )
        LocalStorage.token = ""
        LocalStorage.email = ""
    }
    
    
    internal static func setValue( key :String , values: String )
    {
        LocalStorage.defaults.setValue(  values , forKey: key )
    }
    
    internal static func getValue( key :String  ) -> String?
    {
        
        if let vals = LocalStorage.defaults.value(forKey: key )
        {
            return vals as? String
        }
        return ""
    }
    
    internal static func deleteValue( key: String )
    {
        if let vals = LocalStorage.defaults.value(forKey: key )
        {
            LocalStorage.defaults.setValue(  "" , forKey: key )
        }
    }
    
    /*internal static func setUserObject( userData: User )
     {
     let encodedData = NSKeyedArchiver.archivedData(withRootObject: userData)
     LocalStorage.defaults.set(encodedData, forKey: "userdata")
     LocalStorage.defaults.synchronize()
     }
     
     internal static func getUserObject() -> User?
     {
     if let vals = LocalStorage.defaults.object(forKey: "userdata") as? Data
     {
     NSKeyedUnarchiver.unarchiveObject(with: vals )
     }
     return nil
     }
     */
    
}


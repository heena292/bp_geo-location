//
//  HSHeadingLabel.swift
//  BP-Finannce
//
//  Created by Hardeep Singh on 01/07/17.
//  Copyright © 2017 Hardeep Singh. All rights reserved.
//

import UIKit

class HSHeadingLabel: UILabel {
    
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    
    required init?(coder aDecoder:NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func setup()
    {
        textColor = UIColor.white
        font = UIFont.systemFont(ofSize: 22)
        textAlignment = .center
        
    }
    
}


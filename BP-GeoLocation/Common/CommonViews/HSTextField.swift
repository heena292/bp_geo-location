import Foundation
import UIKit

class HSTextField: UITextField
{
    let padding = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10);
    var id = ""
    
    override var isEnabled: Bool
        {
        willSet {
            backgroundColor = newValue ? UIColor(red: 0.4, green: 0.8, blue: 1.0, alpha: 1)
                : UIColor.lightGray
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame )
        setup()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        // fatalError("init(coder:) has not been implemented")
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        setup()
    }
    
    func setup()
    {
        
        
        
        backgroundColor = UIColor(red: 0.4, green: 0.8, blue: 1.0, alpha: 1)
        
        textColor = UIColor.white
        
        
        
    }
    
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        layer.cornerRadius = frame.height / 2
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return UIEdgeInsetsInsetRect(bounds, padding)
    }
    
}


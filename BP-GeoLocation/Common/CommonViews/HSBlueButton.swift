
import Foundation
import UIKit

class HSBlueButton: UIButton
{
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        //        fatalError("init(coder:) has not been implemented")
        
        
    }
    
    
    
    func setup()
    {
        
        
        backgroundColor = UIColor(red: 0, green: 0.36, blue: 1.0, alpha: 1)
        
        
        setTitleColor(UIColor.white, for: .normal )
        
        titleLabel?.textAlignment = .center
        
        let colors = UIColor(red: 0.4, green: 0.8, blue: 1.0, alpha: 1 )
        setTitleColor(  colors, for: .highlighted )
        
        self.titleLabel?.frame = CGRect(x: 0, y: 0, width: 100, height: 50)
        
        
        
        
    }
    
    override func layoutSubviews() {
        layer.cornerRadius = frame.height / 2
        
        self.titleLabel?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height )
        
    }
    
    
    
}


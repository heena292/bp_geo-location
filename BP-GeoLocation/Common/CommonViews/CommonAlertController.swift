//
//  CommonAlertController.swift
//  BP-Finannce
//
//  Created by Hardeep Singh on 05/08/17.
//  Copyright © 2017 Hardeep Singh. All rights reserved.
//

import Foundation
import UIKit

class CommonAlertController
{
    
    public static func showAlertController( controller: UIViewController, title: String, message: String, btnTitle: String  )
    {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        let action = UIAlertAction(title: btnTitle, style: UIAlertActionStyle.cancel) { (alertAction) in
            
            
        }
        
        alertController.addAction(action)
        
        if let popOverController = alertController.popoverPresentationController
        {
            popOverController.sourceView = controller.view
            popOverController.sourceRect = controller.view.bounds
            
            
        }
        
        controller.present(alertController, animated: true, completion: nil )
    }
    
}


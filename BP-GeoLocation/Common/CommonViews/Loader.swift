//
//  Loader.swift
//  MoveTheBall
//
//  Created by Hardeep Singh on 04/06/16.
//  Copyright © 2016 Hardeep Singh. All rights reserved.
//

import Foundation
import UIKit

class Loader
{
    
    class func createLoader(_ controller: UIViewController) -> UIView
    {
        let container: UIView = UIView()
        let uiView = controller.view
        container.frame = (uiView?.frame)!
        container.center = (uiView?.center)!
        container.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = (uiView?.center)!
        loadingView.backgroundColor =  UIColor(red: 0, green: 0.36, blue: 1, alpha: 1) // UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.center = controller.view.center
        indicator.hidesWhenStopped = true
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                   y: loadingView.frame.size.height / 2);
        
        //indicator.addConstraint(NSLayoutConstraint)
        //   indicator.frame.size.width = 150
        //  indicator.frame.size.height = 150
        //  controller.view.addSubview(indicator)
        
        loadingView.addSubview(indicator)
        container.addSubview(loadingView)
        //controller.view.addSubview(container)
        
        controller.view.insertSubview(container, at: 10000)
        indicator.startAnimating()
        container.isHidden = true
        
        return container
    }
    
    
    class func createLoader(_ view: UIView) -> UIView
    {
        let container: UIView = UIView()
        let uiView = view
        container.frame = (uiView.frame)
        container.center = (uiView.center)
        container.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0.3)
        
        let loadingView: UIView = UIView()
        loadingView.frame = CGRect(x: 0, y: 0, width: 80, height: 80)
        loadingView.center = (uiView.center)
        loadingView.backgroundColor =  UIColor(red: 0, green: 0.36, blue: 1, alpha: 1) // UIColor(red: 0.3, green: 0.3, blue: 0.3, alpha: 1)
        loadingView.clipsToBounds = true
        loadingView.layer.cornerRadius = 10
        
        let indicator:UIActivityIndicatorView = UIActivityIndicatorView(activityIndicatorStyle: .whiteLarge)
        indicator.center = view.center
        indicator.hidesWhenStopped = true
        indicator.frame = CGRect(x: 0.0, y: 0.0, width: 40.0, height: 40.0);
        indicator.center = CGPoint(x: loadingView.frame.size.width / 2,
                                   y: loadingView.frame.size.height / 2);
        
        //indicator.addConstraint(NSLayoutConstraint)
        //   indicator.frame.size.width = 150
        //  indicator.frame.size.height = 150
        //  controller.view.addSubview(indicator)
        
        loadingView.addSubview(indicator)
        container.addSubview(loadingView)
        //controller.view.addSubview(container)
        
        view.insertSubview(container, at: 10000)
        indicator.startAnimating()
        container.isHidden = true
        
        return container
    }
    
    
    /*class func hideLoader()
     {
     
     }
     
     
     class func removeLoader( _ container:UIView )
     {
     //indicator.stopAnimating()
     //container.removeFromSuperview()
     container.isHidden = true
     }*/
    
    
    
}


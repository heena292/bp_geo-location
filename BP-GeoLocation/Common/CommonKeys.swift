

enum UrlKeys {
    
    
    //static let baseUrl = "http://192.168.1.199/kim/sitenew/appdata"
    //static NSString *apiBasicTestUrl = @"http://erp.business-pointers.com/BPServicesTest/";
    
    //static let baseUrl = "http://erp.business-pointers.com/Valcon.WebService.Business/"
    
    //static let getDataUrl = "http://erp.business-pointers.com/Valcon.WebService.Business/"
    
    // this key has no use now
    // we have define in Appdelegate class with name dynamicUrl
    // as dynamic url is coming from server after login
    // so below url is only working at the time of login but getting always from appdelegate dynamic url
    // So no use of below urls
   // static let baseUrl = "http://erp.business-pointers.com/Valcon.WebService.BusinessV8/"
  //  static let getDataUrl = "http://erp.business-pointers.com/Valcon.WebService.BusinessV8/"
   
    static let baseUrl = "https://erp.business-pointers.com/BPServices/"
    static let getDataUrl = "https://erp.business-pointers.com/BPServices/"
    
    
}

enum defaultsKeys {
    
    static let userKey = "userId"
    static let tokenKey = "AuthKey"
    
    static let token = "token"
    static let deviceTypeKey = "devicetype"
    static let action = "action"
    static let deviceTypeValue = "ios"
    
}

enum LoginKeys
{
     static let keyAutoLoginEnabled = "isautologinenabled"
     static let keyTouchEnabled = "istouchenabled"
     static let keyUsername = "hs_username"
    static let keyAutoLoginUserName = "hs_autologin_username"
}

enum ControllersSBID
{
    static let loginViewController = "loginVC"
    static let mainNavigationController = "sbnavigation"
    static let launchScreen = "sblaunchscreen"
    
}

enum LocalStorageKeys
{
    static let locationaccuracy = "locationaccuracy"
    static let locationtime = "locationtime"
}



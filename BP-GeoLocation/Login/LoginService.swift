//
//  LoginService.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 05/04/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import Foundation
import Alamofire

class LoginService
{
    func loginUser(username: String, password: String, callback: @escaping (_ data:JSON)-> Void )
    {
        
        let parameters: Parameters = [ "UserId": username, "Password" : password,
                                       "ProjectId" : "3"
        ]
        //   let headers: HTTPHeaders = [
        //       "Content-Type": "application/json",
        //       "Accept": "application/json"
        //   ]
        
        
        Alamofire.request( "\(UrlKeys.getDataUrl)Login.asmx/GetAuthenticatedUserDetails_", method: .post, parameters: parameters, encoding: JSONEncoding.default , headers: nil ).responseJSON { response in
            debugPrint(response)
            print(parameters)
            // print(response.result.value)
            let datas = JSON( response.result.value )
            // print("hello111")
            // print(datas["d"]["Data"])
            callback(datas)
            
        }
                    
    }
    
   /* func login(username: String, password: String )
    {
        
        
        let parameters: Parameters = [ "UserId": username, "Password" : password,
                                       "ProjectId" : "1"
        ]
        //   let headers: HTTPHeaders = [
        //       "Content-Type": "application/json",
        //       "Accept": "application/json"
        //   ]
        
        
        Alamofire.request( "https://erp.business-pointers.com/Valcon.WebService.BusinessV8/Login.asmx/GetAuthenticatedUserDetails_", method: .post, parameters: parameters, encoding: JSONEncoding.default , headers: nil ).responseJSON { response in
            //debugPrint(response)
            // print(response.result.value)
            let datas = JSON( response.result.value )
            // print("hello111")
            // print(datas["d"]["Data"])
            
            
            if datas["d"]["Result"].intValue == 1 || datas["LoginDetail"].count > 0
            {
                OperationQueue.main.addOperation {
                    
                    self.loaderView?.isHidden = true
                    if username != "" { //self.textUserName.text != "" {
                        LocalStorage.setValue(key: self.keyUsername, values: username  )
                    }
                    
                    // save password in keychain
                    // if touch id enable and success login then we not need to update password
                    // otherwise use filled the password in text field and we are here because of success login so update the password in key chain
                    if password != "" {  //self.textPassword.text != "" {
                        self.MyKeyChainWrapper.mySetObject( password, forKey: "v_Data")
                    }
                    
                    var userData = datas["d"]["Data"]
                    if userData.count <= 0
                    {
                        userData = datas
                    }
                    
                    AppDelegate.user = User.createUser(userData: userData )
                    
                    // AppDelegate.dynamicUrl = userData["LoginDetail"]["AppWebServiceUrl"].stringValue
                    AppDelegate.dynamicUrl = "https://erp.business-pointers.com/valcon.webservice.businessV8/"
                    
                    CommonCoreDataFunctions.deleteCoreDataRecordOldRecords(entityName: "GeoLocation", olderThanDays: 15 )
                    
                    //remove notifications
                    
                    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["loginnotifications"])
                    
                    
                    if let vc = self.storyboard?.instantiateViewController(withIdentifier: "sbnavigation") as? NavigationViewController
                    {
                        UIApplication.shared.keyWindow?.rootViewController = vc
                    }
                    
                    
                }
                
            }
            else
            {
                OperationQueue.main.addOperation {
                    self.loaderView?.isHidden = true
                    CommonAlertController.showAlertController(controller: self, title: "Error", message: datas["d"]["Message"].stringValue, btnTitle: "Ok")
                }
            }
            
            
            
            
        } */
        
        
        
        /* var postString = "{\"UserId\": \"\(username)\",\"Password\": \"\(password)\",\"ProjectId\":1}"
         
         CommonDBOperations.postOperation(action: "Login.asmx/GetAuthenticatedUserDetails_", postString: postString) { (jsonData) in
         
         print("my data")
         print(jsonData)
         let datas = JSON(jsonData)
         
         if datas["d"]["Result"].intValue == 1 || datas["LoginDetail"].count > 0
         {
         OperationQueue.main.addOperation {
         
         self.loaderView?.isHidden = true
         if username != "" { //self.textUserName.text != "" {
         LocalStorage.setValue(key: self.keyUsername, values: username  )
         }
         
         // save password in keychain
         // if touch id enable and success login then we not need to update password
         // otherwise use filled the password in text field and we are here because of success login so update the password in key chain
         if password != "" {  //self.textPassword.text != "" {
         self.MyKeyChainWrapper.mySetObject( password, forKey: "v_Data")
         }
         
         var userData = datas["d"]["Data"]
         if userData.count <= 0
         {
         userData = datas
         }
         AppDelegate.user.authKey = userData["DefaultValue"]["AuthKey"].stringValue
         
         AppDelegate.user.companyCode = userData["DefaultValue"]["CompanyCode"].stringValue
         
         AppDelegate.user.companyId = userData["DefaultValue"]["CompanyId"].stringValue
         
         AppDelegate.user.authKey = userData["DefaultValue"]["CompanyName"].stringValue
         
         AppDelegate.user.countryId = userData["DefaultValue"]["CountryId"].stringValue
         
         AppDelegate.user.countryName = userData["DefaultValue"]["CountryName"].stringValue
         
         AppDelegate.user.currency = userData["DefaultValue"]["Currency"].stringValue
         
         AppDelegate.user.dateFormat = userData["DefaultValue"]["DateFormat"].stringValue
         
         AppDelegate.user.languageId = userData["DefaultValue"]["LanguageId"].stringValue
         
         
         AppDelegate.user.languageText = userData["DefaultValue"]["LanguageText"].stringValue
         
         
         AppDelegate.user.currentCompanyId = userData["LoginDetail"]["CurrentCompanyId"].stringValue
         
         
         
         AppDelegate.user.currentCompanyLogo = userData["LoginDetail"]["CurrentCompanyLogo"].stringValue
         
         
         AppDelegate.user.currentCompanyName = userData["LoginDetail"]["CurrentCompanyName"].stringValue
         
         
         
         AppDelegate.user.currentCompanyId = userData["LoginDetail"]["CurrentCompanyId"].stringValue
         
         
         
         AppDelegate.user.defaultGlobalProjectId = userData["LoginDetail"]["DefaultGlobalProjectId"].stringValue
         
         
         
         AppDelegate.user.departmentId = userData["LoginDetail"]["DepartmentId"].stringValue
         
         AppDelegate.user.employeeCode = userData["LoginDetail"]["EmployeeCode"].stringValue
         
         AppDelegate.user.employeeId = userData["LoginDetail"]["EmployeeId"].stringValue
         
         AppDelegate.user.employeeName = userData["LoginDetail"]["EmployeeName"].stringValue
         
         AppDelegate.user.encryptionHashKey = userData["LoginDetail"]["EncryptionHashKey"].stringValue
         
         
         AppDelegate.user.instanceName = userData["LoginDetail"]["InstanceName"].stringValue
         
         
         AppDelegate.user.locationId = userData["LoginDetail"]["LocationId"].stringValue
         
         
         
         AppDelegate.user.userId = userData["LoginDetail"]["UserId"].stringValue
         
         
         AppDelegate.user.userMasterId = userData["LoginDetail"]["UserMasterId"].stringValue
         
         
         AppDelegate.user.userTypeId = userData["LoginDetail"]["UserTypeId"].stringValue
         
         
         AppDelegate.user.valconClientId = userData["LoginDetail"]["ValconClientId"].stringValue
         
         //url to call
         AppDelegate.dynamicUrl = userData["LoginDetail"]["AppWebServiceUrl"].stringValue
         
         if let vc = self.storyboard?.instantiateViewController(withIdentifier: "sbdashboard") as? DashboardViewController
         {
         UIApplication.shared.keyWindow?.rootViewController = vc
         }
         
         
         }
         
         }
         else
         {
         OperationQueue.main.addOperation {
         self.loaderView?.isHidden = true
         CommonAlertController.showAlertController(controller: self, title: "Error", message: datas["d"]["Message"].stringValue, btnTitle: "Ok")
         }
         }
         
         
         
         } */
   // }
    
    
}

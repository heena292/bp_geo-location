//
//  TopView.swift
//  BP-Finannce
//
//  Created by Hardeep Singh on 01/07/17.
//  Copyright © 2017 Hardeep Singh. All rights reserved.
//

import UIKit

class TopView: UIView {
    
    var logoView = UIImageView()
    var logoWidth: CGFloat = 192
    var logoHeight: CGFloat = 102
    var labelHeading = HSHeadingLabel()
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init( coder: aDecoder )
        //fatalError("init(coder:) has not been implemented")
    }
    
    func setup()
    {
        labelHeading.text = "Login"
        addSubview(logoView)
        addSubview(labelHeading)
        logoView.image = UIImage(named: "logo")
        
        //logoView.layer.borderColor = UIColor.green.cgColor
        //logoView.layer.borderWidth = 5.0
        //print("image \(logoView.image)")
        
        logoView.contentMode = UIViewContentMode.scaleAspectFit
        backgroundColor = UIColor(red: 0, green: 0.36, blue: 1.0, alpha: 1)
        
        
    }
    
    override func layoutSubviews() {
        
        logoView.frame = CGRect(x: (frame.width - logoWidth) / 2, y: (frame.height - logoHeight) / 2, width: logoWidth, height: logoHeight )
        
        labelHeading.frame = CGRect(x: 0, y: frame.height - 40, width: frame.width, height: 40 )
        
    }
    
    
}


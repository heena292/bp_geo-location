//
//  LoginViewController.swift
//  BP-POS
//
//  Created by Hardeep Singh on 01/11/17.
//  Copyright © 2017 Business Pointers. All rights reserved.
//



import UIKit
import LocalAuthentication
import Alamofire
import UserNotifications

extension LoginViewController: UITextFieldDelegate
{
    
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        view.endEditing(true )
    }
    
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
            if self.view.frame.origin.y == 0{
                
                //self.view.frame.origin.y -= keyboardSize.height - 100
                if  ( editingTextField.frame.origin.y + editingTextField.frame.height + 16 )    > keyboardSize.origin.y
                {
                    self.view.frame.origin.y -= keyboardSize.origin.y - ( editingTextField.frame.origin.y + editingTextField.frame.height + 16 )
                }
            }
            
        }
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
      if ((notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue) != nil {
            
            
            //if self.view.frame.origin.y != 0{
            //    self.view.frame.origin.y += keyboardSize.height
            //}
            self.view.frame.origin.y = 0
        }
        
    }
    
    
    public func textFieldDidBeginEditing(_ textField: UITextField){
        editingTextField = textField
    }
    
}

class LoginViewController: UIViewController {
    
    
    
    
    var topView: TopView!
    var textUserName = HSTextField()
    var textPassword = HSTextField()
    var buttonLogin:HSBlueButton!
    var btnKeepLoggedIn = UIButton()
    var btnEnableTouchId = UIButton()
    var labelKeepLoggedIn = UILabel()
    var labelEnableTouchid = UILabel()
    
    var editingTextField = UITextField()
    
    var loaderView: UIView?
    
    var loginModel = LoginModel()
    
    var btnTouchLogin = UIButton()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        
        
        setupViews()
        // Do any additional setup after loading the view.
        //let parameters: Parameters = ["foo": "bar"]
        // var postString = "{\"UserId\": \"\(username)\",\"Password\": \"\(password)\",\"ProjectId\":1}"
        
        //CommonDBOperations.postOperation(action: "Login.asmx/GetAuthenticatedUserDetails_", postString: postString) { (jsonData) in
        
        labelEnableTouchid.isHidden = true
        btnEnableTouchId.isHidden = true
        btnTouchLogin.addTarget(self, action: #selector(touchIDBtnPressed(_:)), for: .touchUpInside)
        
        
        
    }
    
    @objc func touchIDBtnPressed( _ btn: UIButton )
    {
        touchLogin()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        LocalStorage.defaults.synchronize()
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
       
        super.viewWillAppear(animated)
         initNotificationSetupCheck()
       
        btnTouchLogin.isHidden = true
        
        if LocalStorage.getValue(key: LoginKeys.keyTouchEnabled ) == "1"
        {
            btnTouchLogin.isHidden = false
            
            touchLogin()
            //btnEnableTouchId.isHidden = true
            //labelEnableTouchid.isHidden = true
            // go to live server for auto login
            
            //autoLogin()
        }
       
        
        // button touch enabled already fro user when previous logged in
        /*if LocalStorage.getValue(key: keyTouchEnabled) == "1"
        {
            btnEnableTouchId.setImage(UIImage(named: "checkselected" ) , for: UIControlState.normal )
        }
        else
        {
            btnEnableTouchId.setImage(UIImage(named: "check" ) , for: UIControlState.normal )
            
        }
        
        if LocalStorage.getValue(key: keyAutoLoginEnabled) == "1"
        {
            btnKeepLoggedIn.setImage(UIImage(named: "checkselected" ) , for: UIControlState.normal )
        }
        else
        {
            btnKeepLoggedIn.setImage(UIImage(named: "check" ) , for: UIControlState.normal )
            
        } */
        
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewDidLayoutSubviews() {
        
       
        // landscape
        if view.bounds.height < 450
        {
            
            topView.logoView.isHidden = true
            topView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 50 )
            
            topView.layoutIfNeeded()
            
        }
        else
        {
              
            topView.logoView.isHidden = false
            
            topView.frame = CGRect(x: 0, y: 0, width: view.bounds.width, height: 200 )
            topView.layoutIfNeeded()
        }
        let labelWidth: CGFloat = 300
        
        textUserName.frame = CGRect(x: ( view.bounds.width - labelWidth ) / 2 , y: topView.frame.origin.y +  topView.frame.height + 10, width:
            labelWidth, height: 40 )
        
        textPassword.frame = CGRect(x: ( view.bounds.width - labelWidth ) / 2 , y: textUserName.frame.origin.y +  textUserName.frame.height + 10, width:
            labelWidth, height: 40 )
        
        btnTouchLogin.frame = CGRect(x: textPassword.frame.origin.x + textPassword.frame.width - 42 , y: textPassword.frame.origin.y - 2 , width: 44  , height: 44 )
        
        buttonLogin.frame = CGRect(x: ( view.bounds.width - 300 ) / 2, y: textPassword.frame.origin.y +  textPassword.frame.height + 20, width: 300, height: 44 )
        
        
        
        if view.bounds.width  < 385
        {
            btnKeepLoggedIn.frame = CGRect(x: 8, y: buttonLogin.frame.origin.y + buttonLogin.frame.height + 16 , width: 30, height: 30 )
            labelKeepLoggedIn.frame = CGRect(x: btnKeepLoggedIn.frame.origin.x + btnKeepLoggedIn.frame.width + 8, y: btnKeepLoggedIn.frame.origin.y, width: 125, height: 30)
            
            
            btnEnableTouchId.frame = CGRect(x: 8, y: btnKeepLoggedIn.frame.origin.y + btnKeepLoggedIn.frame.height + 10 , width: 30, height: 30 )
            
            labelEnableTouchid.frame = CGRect(x: btnEnableTouchId.frame.origin.x + btnEnableTouchId.frame.width + 8, y: btnEnableTouchId.frame.origin.y, width: 125, height: 30)
            
            
        }
        else
        {
            
            let widths: CGFloat = 400
            btnKeepLoggedIn.frame = CGRect(x:  (view.bounds.width - widths ) / 2  + 16, y: buttonLogin.frame.origin.y + buttonLogin.frame.height + 16 , width: 30, height: 30 )
            labelKeepLoggedIn.frame = CGRect(x: btnKeepLoggedIn.frame.origin.x + btnKeepLoggedIn.frame.width + 8, y: btnKeepLoggedIn.frame.origin.y, width: 125, height: 30)
            
            labelEnableTouchid.frame = CGRect(x:  btnKeepLoggedIn.frame.origin.x  + 200 + 46, y: labelKeepLoggedIn.frame.origin.y, width: 125, height: 30)
            
            btnEnableTouchId.frame = CGRect(x: labelEnableTouchid.frame.origin.x - 40   , y: labelEnableTouchid.frame.origin.y   , width: 30, height: 30 )
            
            
            
        }
        
        
        
    }
    
    func setupViews()
    {
        buttonLogin = HSBlueButton(frame: .zero )
        
        buttonLogin.addTarget(self, action: #selector(loginPressed(_:)), for: .touchUpInside )
        
        topView = TopView()
        topView.logoView.image = UIImage(named: "logo" )
        textPassword.isSecureTextEntry = true
        
        
        
        buttonLogin.setTitle("Login", for: .normal )
        
        textUserName.placeholder = "Username"
        textPassword.placeholder = "Password"
        
        labelKeepLoggedIn.text = "Keep Logged In"
        btnKeepLoggedIn.setImage(UIImage(named: "checknew" ), for: .normal )
        
        if LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled) == "1"
        {
                btnKeepLoggedIn.setImage(UIImage(named: "checknewselected" ), for: .normal )
        }
        
        btnKeepLoggedIn.addTarget(self, action: #selector(btnKeepLoggedInPressed(_:)), for: .touchUpInside)
        
        
        btnTouchLogin.setImage( UIImage(named: "touchicon") , for: .normal )
        
        
        labelEnableTouchid.text = "Enable Touch ID"
        btnEnableTouchId.setImage(UIImage(named: "checknew" ), for: .normal )
     //   btnEnableTouchId.addTarget(self, action: #selector(btnEnableTouchIdPressed(_:)), for: .touchUpInside)
        labelEnableTouchid.textColor = UIColor(red: 0, green: 0.48, blue: 1.0, alpha: 1.0)
        labelKeepLoggedIn.textColor =  UIColor(red: 0, green: 0.48, blue: 1.0, alpha: 1.0)
        labelKeepLoggedIn.isUserInteractionEnabled = true
        let gesture = UITapGestureRecognizer()
        labelKeepLoggedIn.addGestureRecognizer(gesture)
        gesture.addTarget(self, action: #selector(keepLoggedLabelClicked(_:)))
        
        view.addSubview(topView)
        view.addSubview(textUserName)
        view.addSubview(textPassword)
        view.addSubview(buttonLogin)
        view.addSubview(btnKeepLoggedIn)
        view.addSubview(labelKeepLoggedIn)
        view.addSubview(btnEnableTouchId)
        view.addSubview(labelEnableTouchid)
        view.addSubview(btnTouchLogin)
        
        loaderView = Loader.createLoader(self)
    }
    
    
    @objc func keepLoggedLabelClicked( _ obj: Any )
    {
        toggleKeepLoggedIm()
    }
    
    func toggleKeepLoggedIm()
    {
        if LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled ) == "1"
        {
            LocalStorage.setValue(key: LoginKeys.keyAutoLoginEnabled, values: "0")
            btnKeepLoggedIn.setImage( UIImage( named: "checknew" ) , for: UIControlState.normal )
        }
        else
        {
            LocalStorage.setValue(key: LoginKeys.keyAutoLoginEnabled, values: "1")
            btnKeepLoggedIn.setImage( UIImage( named: "checknewselected" ) , for: UIControlState.normal )
        }
        
        //print(" Auto Login Key 545 \(LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled))" )
        
        LocalStorage.defaults.synchronize()
    }
    @objc func btnKeepLoggedInPressed(_ btn: UIButton )
    {
        
       toggleKeepLoggedIm()
    }
    
    
  /*  @objc  func btnEnableTouchIdPressed( _ btn : UIButton )
    {
        
        if LocalStorage.getValue(key: keyTouchEnabled ) == "1"
        {
            LocalStorage.setValue(key: keyTouchEnabled, values: "0")
            btn.setImage( UIImage( named: "check" ) , for: UIControlState.normal )
        }
        else
        {
            LocalStorage.setValue(key: keyTouchEnabled, values: "1")
            btn.setImage( UIImage( named: "checkselected" ) , for: UIControlState.normal )
        }
        
    } */
    
    @objc func loginPressed( _ btn: UIButton )
    {
        
      let userName = textUserName.text! //"Hardeep@HRMSTestDBV8.com"
      let pass = textPassword.text! //"value@1234"
        
        let msg = check()
        if msg == "1" {
            
            loaderView?.isHidden = false
            loginModel.login(username: userName, password: pass, callback: {
               
                OperationQueue.main.addOperation {
                
                    self.loaderView?.isHidden = true
                    if( self.loginModel.user == nil )
                    {
                        let msg = "Username or Password is wrong."
                        CommonAlertController.showAlertController(controller: self, title: "Error!", message: msg, btnTitle: "Ok")
                    }
                        
                    else
                    {
                        if LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled ) == "1"
                        {
                            LocalStorage.setValue(key: LoginKeys.keyAutoLoginUserName, values:  userName )
                        }
                        else
                        {
                            LocalStorage.setValue(key: LoginKeys.keyAutoLoginUserName, values:  "" )

                        }
                        LocalStorage.defaults.synchronize()
                        
                        
                    
                        
                        if let appDelegate = UIApplication.shared.delegate as? AppDelegate
                        {
                            appDelegate.setDashboardAsRootController()
                        }
                    }
                    
                }
                
                
                
                
            })
            //login(username: userName, password: pass )
        }
        else
        {
            CommonAlertController.showAlertController(controller: self, title: "Error!", message: msg, btnTitle: "Ok")
            
        }
        
    }
    
    func check() -> String
    {
        var msg = ""
        if textUserName.text == ""
        {
            msg = "Username is required.\n"
        }
        if textPassword.text == ""
        {
            msg += "Password is required.\n"
        }
        
        if msg.characters.count > 5
        {
            return msg
        }
        
        return "1"
    }
    
    
    
    func loginCallback(_ jsonData: NSDictionary, isAutoLogin: Bool)
    {
        /*OperationQueue.main.addOperation {
         
         print("login Data")
         print(jsonData)
         let datas = JSON(jsonData)
         
         if self.loaderView != nil {
         self.loaderView?.isHidden = true
         }
         if let success = jsonData.value(forKey: "success")
         {
         
         if (success as AnyObject).intValue == 1
         {
         print("loginuserid")
         print(datas["id"].intValue)
         
         
         AppDelegate.currentLoginUserId = datas["id"].intValue
         AppDelegate.dateString = ""
         AppDelegate.updateMealPlannerFromOnlinData = false
         AppDelegate.userType = datas["usertype"].intValue
         AppDelegate.userDisplayname = datas["name"].stringValue
         AppDelegate.isUserLoggedIn = true
         
         //  print(jsonData)
         if let email = jsonData.value(forKey: "email") as? NSString
         {
         
         LocalStorage.update_email(email as String)
         
         }
         
         // if no autologin then we need to store
         // username and password to make it workable
         // with touchid
         
         if isAutoLogin == false {
         
         // If touch login enabled and username not empty in NSuserdefaults then we are already filling txtusername with username value so username is setting
         // otherwise user is self filling to login so txtusername has a value
         
         
         if self.txtUsername.text != "" {
         LocalStorage.setValue(key: self.keyUsername, values: self.txtUsername.text!  )
         }
         
         // save password in keychain
         // if touch id enable and success login then we not need to update password
         // otherwise use filled the password in text field and we are here because of success login so update the password in key chain
         if self.txtPassword.text != "" {
         
         self.MyKeyChainWrapper.mySetObject( self.txtPassword.text!, forKey: "v_Data")
         }
         }
         
         if let token = jsonData.value(forKey: "devicekey") as? NSString
         {
         // print("hardeepupdatetoken")
         // print(token)
         LocalStorage.update_token(token as String )
         
         
         }
         
         
         
         if AppDelegate.userType == 0 // free user show membership alert
         {
         
         
         //  let sb = UIStoryboard(name: "SecondStoryboard", bundle: nil )
         
         // if sb != nil
         // {
         
         
         
         
         if let vc = self.storyboard?.instantiateViewController(withIdentifier: "mealplannermaintabbar" ) as? MealPlannerMainTabBarViewController
         {
         let navVC = UINavigationController(rootViewController: vc )
         
         //navVC.modalPresentationStyle = .fullScreen
         vc.pushedFromLoginView = true
         
         navVC.navigationBar.isTranslucent = false
         navVC.navigationBar.tintColor = UIColor.white
         navVC.navigationBar.barStyle = UIBarStyle.black
         // navVC.navigationBar.topItem?.title = "Het"
         navVC.navigationBar.barTintColor = UIColor.rgb(red: 255, green: 67, blue: 153, alpha: 1.0 )
         
         
         
         self.show(navVC, sender: self )
         }
         
         
         //}
         }
         
         else
         {
         if let vc = self.storyboard?.instantiateViewController(withIdentifier: "mealplannermaintabbar") as? MealPlannerMainTabBarViewController
         {
         
         vc.pushedFromLoginView = true
         //self.moreNavigationController.title = "bablu"
         self.show(vc, sender: self )
         }
         }
         /* else if let vc = self.storyboard?.instantiateViewController(withIdentifier: "sbidmemberhome") as? MemberHomeViewController
         {
         
         
         self.show(vc, sender: self )
         } */
         
         
         
         }
         else
         
         {
         if isAutoLogin == false
         {
         AlertMsgs.alertBox("Warning", alertMsg: "Invalid Username or Password.", myViewController: self )
         }
         }
         
         }
         else
         {
         if isAutoLogin == false
         {
         
         AlertMsgs.alertBox("Warning", alertMsg: "Invalid Username or Password.", myViewController: self )
         
         }
         
         }
         } */
    }
    
    func touchLogin()
    {
        
        var laContext: LAContext = LAContext()
        var err: NSError?
        
        var myLocalizedReasonString = "Please auntheticate by Touch/Face ID for login"
        if laContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &err )
        {
            if err == nil
            {
                
                var usernames = LocalStorage.getValue(key:LoginKeys.keyUsername)
                print("username: \(usernames!)")
                
                if usernames != "" && LocalStorage.getValue(key: LoginKeys.keyTouchEnabled) == "1"
                {
                    self.textUserName.text = usernames
                    // now check touch login
                    laContext.evaluatePolicy(.deviceOwnerAuthentication, localizedReason: myLocalizedReasonString, reply: { (success, errors) in
                        
                        if success
                        {
                            OperationQueue.main.addOperation {
                                
                                
                                let appDelegate = UIApplication.shared.delegate as! AppDelegate
                                let pass = appDelegate.MyKeyChainWrapper.myObject(forKey: "v_Data")
                                self.loaderView?.isHidden = false
                                self.loginModel.login(username: usernames! , password: pass as! String, callback: {
                                    
                                    OperationQueue.main.addOperation {
                                        self.loaderView?.isHidden = true
                                    if self.loginModel.user != nil
                                    {
                                        if LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled) == "1"
                                        {
                                            LocalStorage.setValue(key: LoginKeys.keyAutoLoginUserName, values: usernames!)
                                        }
                                        else
                                        {
                                            LocalStorage.setValue(key: LoginKeys.keyAutoLoginUserName, values: "" )
                                        }
                                        
                                        appDelegate.setDashboardAsRootController()
                                    }
                                        else
                                    {
                                        let msg = "Authentication error."
                                        CommonAlertController.showAlertController(controller: self, title: "Error!", message: msg, btnTitle: "Ok")
                                    }
                                        
                                    }
                                    
                                })
                                
                            }
                        }
                    })
                    
                }
            }
                // as user hasn't touch id enable from settings
            else
            {
                OperationQueue.main.addOperation {
                    
                    self.btnEnableTouchId.isHidden = true
                    self.labelEnableTouchid.isHidden = true
                }
            }
        }
        else
        {
            self.btnEnableTouchId.isHidden = true
            self.labelEnableTouchid.isHidden = true
            
        }
        
    }
    
    
   /* func autoLogin()
    {
        print("In autologin")
        
        
        var usernames = LocalStorage.getValue(key: keyUsername)
        print("username: \(usernames!)")
        
        if usernames != ""
        {
            self.textUserName.text = usernames
            let pass = self.MyKeyChainWrapper.myObject(forKey: "v_Data")
            
            self.login( username: usernames!     , password: pass as! String )
            
            
        }
    } */
    
    
    func loginNotification()
    {
        AppDelegate.sendLoginNotification()
    }
    
    
    
    func initNotificationSetupCheck() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound])
        { (success, error) in
            if success {
            
                //self.loginNotification()
                
                print("Permission Granted")
                
                
            } else {
                print("There was a problem!")
            }
        }
    }
    
    
    
}

//
//  LoginModel.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 05/04/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import Foundation
import UIKit

class LoginModel
{
    var loginService: LoginService!
    var user:User?
    
    
    init() {
        loginService = LoginService()
        user = nil
    }
    
    func login( username: String, password: String, callback: @escaping ()->Void  )
    {
        
        loginService.loginUser(username: username, password: password) { (datas) in
            
            print(datas)
            if datas["d"]["Result"].intValue == 1 || datas["LoginDetail"].count > 0
            {
               
                var userData = datas["d"]["Data"]
                if userData.count <= 0
                {
                    userData = datas
                }
                
                LocalStorage.setValue(key: LoginKeys.keyUsername, values: username )
                
                if password != "" {  //self.textPassword.text != "" {
                    if let delegate = UIApplication.shared.delegate as? AppDelegate
                    {
                        delegate.MyKeyChainWrapper.mySetObject( password, forKey: "v_Data")
                    }
                    
                }
                
                let dUrl = userData["LoginDetail"]["AppWebServiceUrl"].stringValue
                AppDelegate.dynamicUrl = dUrl.replacingOccurrences(of: "http:", with: "https:")
                
                self.user = User.createUser(userData: userData )
                AppDelegate.user = self.user
                AppDelegate.isJustLoggedIn = true
            }
            
            callback()
            
        }
        
    }
    
}

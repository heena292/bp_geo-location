//
//  AppDelegate.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 17/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit
import CoreData
import UserNotifications

@available(iOS 10.0, *)
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {

    var window: UIWindow?
    var MyKeyChainWrapper = KeychainWrapper()
    static var user: User?
    static var dynamicUrl = "https://erp.business-pointers.com/BPServices/"
    static var isJustLoggedIn = true

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        UNUserNotificationCenter.current().delegate = self
        
        NotificationCenter.default.addObserver(self, selector: #selector(onAppWillTerminate(_:)), name:   NSNotification.Name.UIApplicationWillTerminate, object: nil)
        
        //NSNotificationCenter.defaultCenter().addObserver(self, selector:#selector(AppDelegate.onAppWillTerminate(_:)), name:UIApplicationWillTerminateNotification, object:nil)
        
       // print("launched")
        
      UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
      UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        autoLoginCode()
        
        return true
    }

    
    func autoLoginCode()
    {
        //auto login code if different controller
        print("active")
        // as user is not logged in so check if stay logged in enables
        print(" Auto Login Key 550 \(LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled))" )
        if AppDelegate.user == nil
        {
            print("app user is nil")
            // this means auto login enabled
            
            let isAutoLoginEnabled = LocalStorage.getValue(key: LoginKeys.keyAutoLoginEnabled  )
            let usernames =  LocalStorage.getValue(key: LoginKeys.keyAutoLoginUserName )
            //LocalStorage.getValue(key: LoginKeys.keyUsername)
            
            var pass = ""
            if let p  = self.MyKeyChainWrapper.myObject(forKey: "v_Data") as? String
            {
                pass = p
            }
            
            // no auto login parameters set
            if isAutoLoginEnabled != "1" || usernames == "" || pass == ""
            {
                setLoginViewAsRootController()
                return
            }
            setLaunchScreenAsRootView()
            
            var loginModel = LoginModel()
            
            loginModel.login(username: usernames!, password: pass , callback: {
                
                // login successfull
                OperationQueue.main.addOperation {
                    
                    if loginModel.user != nil
                    {
                        //AppDelegate.user = loginModel.user
                        
                        self.setDashboardAsRootController()
                    }
                    else
                    {
                        //AppDelegate.isJustLoggedIn = false
                        self.setLoginViewAsRootController()
                    }
                }
                
            })
            
            
            
        }
        else
        {
            print("app user is filled")
            if let rootVC = self.window?.rootViewController as? NavigationViewController
            {
                print("root vc found")
            }
            else
            {
                print("root vc not found")
                self.setDashboardAsRootController()
            }
            
            
        }
        
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
        //means user is not logged in so we have to change
        //make launch screen as root view controller
       
        
        
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print("deactive")
        if AppDelegate.user == nil
        {
            print("user is nil")
            setLaunchScreenAsRootView()
            AppDelegate.sendLoginNotification()
            
        }
        
        LocalStorage.defaults.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        
        autoLoginCode()
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        
        
        
    }
    
    func setLoginViewAsRootController()
    {
        //time set root view controller
        let sb = UIStoryboard(name: "Main", bundle: nil )
        if let vc = sb.instantiateViewController(withIdentifier: ControllersSBID.loginViewController ) as? LoginViewController
        {
            self.window?.rootViewController = vc
            //UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    
    func setDashboardAsRootController()
    {
        //time set root view controller
        let sb = UIStoryboard(name: "Main", bundle: nil )
        if let vc = sb.instantiateViewController(withIdentifier: ControllersSBID.mainNavigationController) as? NavigationViewController
        {
            self.window?.rootViewController = vc
            //UIApplication.shared.keyWindow?.rootViewController = vc
        }
    }
    
    func setLaunchScreenAsRootView()
    {
        let sb = UIStoryboard(name: "LaunchScreen", bundle: nil )
        if let vc = sb.instantiateViewController(withIdentifier: ControllersSBID.launchScreen ) as? UIViewController
        {
            self.window?.rootViewController = vc
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
        self.saveContext()
    }

    // MARK: - Core Data stack

    @available(iOS 10.0, *)
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
        */
        let container = NSPersistentContainer(name: "BP_GeoLocation")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                 
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    
    


    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }

    
    
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        print("Did recieve response: \(response.notification.request.identifier)")
        //response.actionIdentifier == "loginnotifications" &&
        if  response.notification.request.identifier == "loginnotifications" && AppDelegate.user != nil {
            print("removing notification")
    UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [response.notification.request.identifier])
        }
       completionHandler()
    }
    
    public static func sendLoginNotification()
    {
        print("notification sent")
        //UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: ["loginnotifications"])
        
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
        
        let notification = UNMutableNotificationContent()
        notification.title = "Reminder for Login!"
        notification.subtitle = "You are not logged in"
        notification.body = "Please login to app to preserve your travel history."
        notification.sound = UNNotificationSound.default()
        
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 60 * 15, repeats: true)
        let request = UNNotificationRequest(identifier: "loginnotifications", content: notification, trigger: notificationTrigger)
        
        UNUserNotificationCenter.current().add(request, withCompletionHandler: nil)
    
    }
    
    @objc func onAppWillTerminate( _ notification:NSNotification)
        {
            
           // print("onAppWillTerminate")
            AppDelegate.sendLoginNotification()
            LocalStorage.defaults.synchronize()
            
        }
    
    
}


//
//  TrackingViewController.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 21/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit
import MapKit


class TrackingViewController: UIViewController, MKMapViewDelegate {

    
    @IBOutlet weak var dateBtn: UIButton!
    @IBOutlet weak var mapView: MKMapView!
    
    let dtFormatterForMapAnnotations = DateFormatter()
    @IBAction func dateButtonClicked(_ sender: Any) {
        
        
        
    }
    
    @IBAction func nextDateButtonClicked(_ sender: Any) {
        
        dtFormatterForMapAnnotations.timeStyle = .medium
        dtFormatterForMapAnnotations.dateStyle = .none
        
        if let currentDt = dateBtn.currentTitle {
            dateFormatter.dateFormat = "MM/dd/yyyy"
            if let dates = dateFormatter.date(from:  currentDt ) {
            
            var dayComp = DateComponents()
            dayComp.day = 1
            let date = Calendar.current.date(byAdding: dayComp, to: dates)
            //Calendar.current.component(.weekday, from: date!)
                updateMap(date: date! )
                dateBtn.setTitle( "\(dateFormatter.string(from: date!))", for: .normal )
            }
        }
        
        
        
    }
    
    @IBAction func previousDateButtonClicked(_ sender: Any) {
        
        if let currentDt = dateBtn.currentTitle {
            dateFormatter.dateFormat = "MM/dd/yyyy"
            if let dates = dateFormatter.date(from:  currentDt ) {
                
                var dayComp = DateComponents()
                dayComp.day = -1
                let date = Calendar.current.date(byAdding: dayComp, to: dates)
                //Calendar.current.component(.weekday, from: date!)
                updateMap(date: date! )
                
                dateBtn.setTitle( "\(dateFormatter.string(from: date!))", for: .normal )
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

         mapView.delegate = self
        
        // Do any additional setup after loading the view.
        
        self.parent?.title = "Tracking History"
        
    }
    
    let dateFormatter = DateFormatter()
    override func viewWillAppear(_ animated: Bool) {
        
        dateFormatter.dateFormat = "MM/dd/yyyy"
        
        dateBtn.setTitle("\(dateFormatter.string(from: Date()))", for: .normal )
        
        updateMap(date: Date() )
        
        /*let annotations = getMapAnnotations(date: Date())
        // Add mappoints to Map
        let lastLocation = annotations[annotations.count-1]
        zoomToRegion(lat: lastLocation.latitude , lng: lastLocation.longitude )
        mapView.addAnnotations(annotations)
        
        
        // Connect all the mappoints using Poly line.
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for annotation in annotations {
            points.append(annotation.coordinate)
        }
        var polyline = MKPolyline(coordinates: &points, count: points.count)
        mapView.add(polyline) */
        
        
        
    }
    
    
    func updateMap(date:Date )
    {
        let annotations = getMapAnnotations(date: date)
        // Add mappoints to Map
        if annotations.count > 0 {
        let lastLocation = annotations[annotations.count-1]
        zoomToRegion(lat: lastLocation.latitude , lng: lastLocation.longitude )
        mapView.addAnnotations(annotations)
        
        
        // Connect all the mappoints using Poly line.
        
        var points: [CLLocationCoordinate2D] = [CLLocationCoordinate2D]()
        
        for annotation in annotations {
            points.append(annotation.coordinate)
        }
        var polyline = MKPolyline(coordinates: &points, count: points.count)
        mapView.add(polyline)
        }
        else
        {
            let allAnnotations = self.mapView.annotations
            self.mapView.removeAnnotations(allAnnotations)
            let polyline = mapView.overlays
            mapView.removeOverlays(polyline)
            
        }
    }

    //MARK:- MapViewDelegate methods
    
    func mapView(_ mapView: MKMapView, rendererFor overlay: MKOverlay) -> MKOverlayRenderer {
        if overlay is MKOverlay
        {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        
        
        return MKOverlayRenderer()
    }
    
    /*func mapView(mapView: MKMapView!, rendererForOverlay overlay: MKOverlay!) -> MKOverlayRenderer! {
        if overlay is MKPolyline {
            var polylineRenderer = MKPolylineRenderer(overlay: overlay)
            polylineRenderer.strokeColor = UIColor.blue
            polylineRenderer.lineWidth = 5
            return polylineRenderer
        }
        
        return nil
    } */
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    func zoomToRegion(lat: Double, lng: Double) {
        
        let location = CLLocationCoordinate2D(latitude: lat, longitude: lng )
        
        let region = MKCoordinateRegionMakeWithDistance(location, 3000, 5000)
        
        mapView.setRegion(region, animated: true)
    }
    
    
    func getMapAnnotations(date:Date) -> [Station] {
    var annotations:Array = [Station]()
    
    //load plist file
   /* var stations: NSArray?
    if let path = NSBundle.mainBundle().pathForResource("stations", ofType: "plist") {
    stations = NSArray(contentsOfFile: path)
    } */
        
        let records = CommonCoreDataFunctions.fetchLocationsByDate(entityName: "GeoLocation", date: date )
        
        if records != nil {
            if records!.count > 0 {
                for record in records!
                {
                    if let rc = record as? CoreDataGeoLocation
                    {
                      //  print(rc.userid)
                      //  print(rc.latitude)
                      //  print(rc.longitude)
                      //  print(rc.adddate)
                        
                        let annotation = Station(latitude:  rc.latitude.doubleValue  , longitude: rc.longitude.doubleValue)
                        if let add = rc.address as? String {
                        
                            var dates = ""
                             if let dt = rc.adddate as? Date
                            {
                                dtFormatterForMapAnnotations.dateFormat = "hh:mm a"
                               
                                if let datesStr = dtFormatterForMapAnnotations.string(from: dt) as? String
                                {
                                    
                                    
                                    dates = "\(datesStr), "
                                }
                            }
                            
                            
                            
                            annotation.title = "\(dates)\(add)"
                        }
                        //item.valueForKey("title") as? String
                        annotations.append(annotation)
                        
                    }
                    
                }
            }
        }
    
        return annotations
    }

}

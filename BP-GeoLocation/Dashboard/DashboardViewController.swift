//
//  DashboardViewController.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 19/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire
import LocalAuthentication
import CoreMotion


class DashboardViewController: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate {

    
    @IBOutlet weak var map: MKMapView!
    let newPin = MKPointAnnotation()
    
    var locationManager = CLLocationManager()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.title = "Dashboard"
        NotificationCenter.default.addObserver(self, selector: #selector(stopTracking), name: Notifications.myNotification.name , object: nil )
        
       // locationManager.requestAlwaysAuthorization()
        
        
       self.parent?.title = "Dashboard"
        
        promptTouchLogin()
      
      
      
      
     // getUserMotionType()
      
      
        
        
      //  self.locationManager.startUpdatingLocation()
    }
  
  func getUserMotionType()
  {
    if CMMotionActivityManager.isActivityAvailable()
    {
      let mAM = CMMotionActivityManager()
      
      mAM.startActivityUpdates(to: OperationQueue.main) { (activity1 ) in
        
        let activity = activity1!
        print("Use walking confidence \(activity.confidence)")
        if activity.automotive
        {
          print("User walk type: Automotive")
        }
        else  if activity.cycling
        {
          print("User walk type: Cycle")
        }
        else  if activity.running
        {
          print("User walk type: Running")
        }
        else  if activity.walking
        {
          print("User walk type: walking")
        }
        
      }
    }
    else
    {
      print("No processor available")
    }
  }
    
    func promptTouchLogin()
    {
        if LocalStorage.getValue(key: LoginKeys.keyTouchEnabled) != "1"
        {
            print("inside prompt 1")
            var laContext: LAContext = LAContext()
            var err: NSError?
            
            var myLocalizedReasonString = "Please auntheticate by Touch/Face ID for login"
            if laContext.canEvaluatePolicy(.deviceOwnerAuthentication, error: &err )
            {
                print("inside prompt 2")
                if err == nil
                {
                    print("inside prompt 3")
                var alert = UIAlertController(title: "Touch ID", message: "Enable Login With Touch/Face ID", preferredStyle: .alert )
                
                var alertAction = UIAlertAction(title: "Enable", style: .default, handler: { (action) in
                    
                    LocalStorage.setValue(key: LoginKeys.keyTouchEnabled, values: "1")
                    LocalStorage.defaults.synchronize()
                })
                var cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alertAction) in
                    
                })
                
                alert.addAction( cancelAction )
                alert.addAction(alertAction)
                present(alert, animated: true, completion: nil)
                }
                print("inside prompt 4")
                print(err)
            }
            else
            {
                print("inside prompt 5")
                print(err)
            }
            
        }
    }
    
    @objc func stopTracking()
    {
        print("Location updates stopped")
        locationManager.stopUpdatingLocation()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        startReceivingLocationChanges()
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            // Location updates are not authorized.
            manager.stopUpdatingLocation()
            return
        }
        // Notify the user of any errors.
    }
    
    
    var isFirstTimeLocation = true;
    func locationManager(_ manager: CLLocationManager,  didUpdateLocations locations: [CLLocation]) {
        let lastLocation = locations.last!
        
         print("current position: \(lastLocation.coordinate.longitude) , \(lastLocation.coordinate.latitude)")
        // Do something with the location.
      
      
        //check if coordinates are consistent
        if (lastLocation.horizontalAccuracy < 0  && isFirstTimeLocation == false ) {
            return;
        }
      
     // getUserMotionType()
        var interval:TimeInterval =  lastLocation.timestamp.timeIntervalSinceNow
        
            //NSTimeInterval interval = [newLocation.timestamp timeIntervalSinceNow];
        //check against absolute value of the interval
        if (abs(interval)<30 || isFirstTimeLocation == true ) {
            //DO STUFF
            //update server here
           // print("dostuff")
            
            let center = CLLocationCoordinate2D(latitude: lastLocation.coordinate.latitude, longitude: lastLocation.coordinate.longitude)
           // let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            let region =  MKCoordinateRegionMakeWithDistance( center, 7000, 9000)
           
            self.map.setRegion(region, animated: true)
            newPin.coordinate = lastLocation.coordinate
            map.addAnnotation(newPin)
            
            //save locations in coredata
            
            GeoLocationManager.getAddressFromLocation(longitude: lastLocation.coordinate.longitude, latitude: lastLocation.coordinate.latitude, callBack: { (address) in
           
               let distanceMeters = self.getDistanceFromLastLocation(currentLocation: lastLocation )
              
              // above line must be before below line
              CommonCoreDataFunctions.saveGeoLocation(entityName: "GeoLocation", userId: AppDelegate.user?.userId ?? "", longitude: lastLocation.coordinate.longitude, latitude: lastLocation.coordinate.latitude, address: address )
              
              
              //get distance from last location for current date
             
             
              
              self.saveLocationToServer(location: lastLocation, address: address, distance: distanceMeters  )
                
            })
            
        }
        
        isFirstTimeLocation = false
    }
  
  func getDistanceFromLastLocation(currentLocation: CLLocation ) -> Double
  {
    let records = CommonCoreDataFunctions.fetchLatestRecordForCurrentDate(entityName: "GeoLocation", date: Date() )
    var distanceInMeters = 0.0
    
    
    if records != nil {
      if records!.count > 0 {
        for record in records!
        {
          if let rc = record as? CoreDataGeoLocation
          {
            print("latest_geo_location Date: \(rc.adddate) | Lng == \(rc.longitude) | Lat == \(rc.latitude)")
            let oldLocation =  CLLocation(latitude: rc.latitude.doubleValue, longitude: rc.longitude.doubleValue)
             distanceInMeters = currentLocation.distance(from: oldLocation)
            print("My distance \(distanceInMeters)")
          }
        }
      }
    }
    
    return distanceInMeters
  }
    
    
   
    
    func startReceivingLocationChanges() {
        
        var locationAccuracy: Double = 100.0
        if LocalStorage.getValue(key: LocalStorageKeys.locationaccuracy ) != ""
        {
            print("Localstorage location accuracy \(LocalStorage.getValue(key: LocalStorageKeys.locationaccuracy ))")
            let vl = LocalStorage.getValue(key: LocalStorageKeys.locationaccuracy )!
            print("accuracy \(vl)")
            locationAccuracy = Double( vl )  ?? 100.0
        }
        
        
        
        print("Location accuracy = \(locationAccuracy)")
        
        // Configure and start the service.
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        locationManager.pausesLocationUpdatesAutomatically = false
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.distanceFilter =  locationAccuracy //100.0 //locationAccuracy as! CLLocationDistance  // In meters.
        locationManager.delegate = self
        
         if (CLLocationManager.locationServicesEnabled()) {
            
        
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
            locationManager.pausesLocationUpdatesAutomatically = false
            locationManager.distanceFilter = locationAccuracy  //100.0 //locationAccuracy as! CLLocationDistance  // In meters.
             locationManager.allowsBackgroundLocationUpdates = true
            locationManager.requestAlwaysAuthorization()
            
            //locationManager.requestWhenInUseAuthorization()
        
        }
        
         locationManager.requestAlwaysAuthorization()
        //locationManager.requestWhenInUseAuthorization()
        self.map.delegate = self
       // self.map.showsUserLocation = true
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        
        
         DispatchQueue.main.async {
            self.locationManager.startUpdatingLocation()
        }
        
        
        // Do not start services that aren't available.
        /*if !CLLocationManager.locationServicesEnabled() {
            // Location services is not available.
            return
        }*/
        
        /*let authorizationStatus = CLLocationManager.authorizationStatus()
        if authorizationStatus == CLAuthorizationStatus.notDetermined
        {
            locationManager.requestAlwaysAuthorization()
        } */

        
      /*  if authorizationStatus != .authorizedWhenInUse && authorizationStatus != .authorizedAlways {
            // User has not authorized access to location information.
            return
        } */
        
        
        
        /*locationManager.startUpdatingLocation()*/
       
    }
    
  func saveLocationToServer( location: CLLocation, address: String, distance: Double )
    {
       // print(AppDelegate.user)
        
      //  GeoLocationManager.getAddressFromLocation(longitude: location.coordinate.longitude, latitude: location.coordinate.latitude) { (address) in
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy HH:mm:ss"
            let dateString = dateFormatter.string(from: Date() )
        
        var lat = 0.0
        var lng = 0.0
        if let lats = location.coordinate.latitude as? Double
        {
            lat = lats
        }
        
        if let lngs = location.coordinate.longitude as? Double
        {
            lng = lngs
        }
        //"Devicetype":"android","Distance":0
             let parameters: Parameters =  [
                "oEntities":
                    [ [ "GeographicCoordinatesToObjectMappingId" : "0",
                      "ObjectId" : "0",
                      "ObjectTypeId" : "0",
                      "Latitude" : "\(lat)",
                      "Longitude" : "\(lng)",
                      "Location" :  "\(address)",
                      "CoordinateRecordedDateTime" : "",
                      "CoordinateBroadcastedDateTime" : "",
                      "OtherSysRecordId" : "",
                      "CompanyId" : "0",
                      "IsActive" : "true",
                      "Status" : "",
                      "IsDeleted" : "false",
                      "CreatedUserId" : "0",
                      "CreatedDate": "",
                      "CreatedIP": "",
                      "UpdatedUserId" : "0",
                      "UpdatedDate" : "",
                      "UpdatedIP": "",
                      "Devicetype":"ios",
                      "Distance": "\(distance)"
                      ]],
                
                "userId": "\(AppDelegate.user!.userId)",
                "globalProjectId": "\(AppDelegate.user!.defaultGlobalProjectId)",
                "AuthKey": "\(AppDelegate.user!.authKey)"
            ]
            
      
      print(" Server param \(parameters)")
            
  Alamofire.request( "\(AppDelegate.dynamicUrl)GeoCoordinates.asmx/Add_FieldData",  method: .post, parameters: parameters , encoding: JSONEncoding.default  , headers: nil ).responseJSON(completionHandler: { (response) in
    
   
                //debugPrint( response.request?.httpBody)
    
   // print(   String(data: (response.request?.httpBody)!, encoding: String.Encoding.utf8 ) )
               // print("Request: \(String(describing: response.request))")
               // print(response)
                let datas = JSON( response.result.value )
                print(datas)
                print(datas["d"]["Data"])
                
                
                if datas["d"]["Result"].intValue == 1 || datas["LoginDetail"].count > 0
                {
                    OperationQueue.main.addOperation {
                        
                        
                    }
                }
                
            
                
            })
            
            
            
              
            
            
        //}
     
        
    }
    
    
}

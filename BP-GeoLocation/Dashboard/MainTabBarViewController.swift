//
//  MainTabBarViewController.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 22/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit

protocol NotificationName {
    var name: Notification.Name { get }
}

extension RawRepresentable where RawValue == String, Self: NotificationName {
    var name: Notification.Name {
        get {
            return Notification.Name(self.rawValue)
        }
    }
}

enum Notifications: String, NotificationName {
    case myNotification
}

class MainTabBarViewController: UITabBarController {

    
    @IBAction func actionLogout(_ sender: Any) {
        
        AppDelegate.user = nil
        
        if let vc = storyboard?.instantiateViewController(withIdentifier: "loginVC") as? LoginViewController
        {
            LocalStorage.remove_data()
            //LocalStorage.deleteValue(key: "hs_username")
            LocalStorage.deleteValue(key: LoginKeys.keyAutoLoginEnabled )
            LocalStorage.deleteValue(key: LoginKeys.keyAutoLoginUserName )
            
            UIApplication.shared.keyWindow?.rootViewController = vc
            
            
            NotificationCenter.default.post( NSNotification(name: Notifications.myNotification.name , object: nil ) as Notification )
           
            
        }
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

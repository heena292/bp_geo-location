//
//  CommonCoreDataFunctions.swift
//  KimBensen
//
//  Created by Hardeep Singh on 25/11/16.
//  Copyright © 2016 Hardeep Singh. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CommonCoreDataFunctions
{
    
    static var appDelegate: AppDelegate!
    
    static var managedContext: NSManagedObjectContext!
    
    
    
    class func setupContext()
    {
        if CommonCoreDataFunctions.appDelegate == nil {
            CommonCoreDataFunctions.appDelegate  = UIApplication.shared.delegate as! AppDelegate
            
            CommonCoreDataFunctions.managedContext = CommonCoreDataFunctions.appDelegate.persistentContainer.viewContext
        }
    }
    
    class func getContext() -> NSManagedObjectContext
    {
        CommonCoreDataFunctions.setupContext()
        
        return CommonCoreDataFunctions.managedContext
        
    }
    
    
    class func deleteCoreDataAllRecord( managedContext:NSManagedObjectContext,  entityName: String )
    {
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        
        do {
            let items = try managedContext.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                managedContext.delete(item)
            }
            
            // Save Changes
            try managedContext.save()
            
        } catch {
            // Error Handling
            // ...
        }
    }
    
  /*  class func deleteCoreDataRecord(  entityName:String, foodId: Int, foodFromTable : Int   )
    {
        CommonCoreDataFunctions.setupContext()
        
        let managedContext =  CommonCoreDataFunctions.managedContext
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        
        do {
            let items = try managedContext?.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                if let fd = item as? CoreDataRecentlyUsedFoods
                {
                    
                    if Int(fd.foodid) ==  foodId && Int(fd.foodfromtable) == foodFromTable
                    {
                        managedContext?.delete(item)
                    }
                }
            }
            
            // Save Changes
            try managedContext?.save()
            
        } catch {
            // Error Handling
            // ...
        }
    }
    
    */
    /*class func isFoodExisted( entityName: String, foodId: Int, foodFromTable: Int  ) -> Bool
    {
        CommonCoreDataFunctions.setupContext()
        
        let managedContext =  CommonCoreDataFunctions.managedContext
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        // Configure Fetch Request
        fetchRequest.includesPropertyValues = false
        
        do {
            let items = try managedContext?.fetch(fetchRequest) as! [NSManagedObject]
            
            for item in items {
                if let fd = item as? CoreDataRecentlyUsedFoods
                {
                    
                    if Int(fd.foodid) ==  foodId && Int(fd.foodfromtable) == foodFromTable
                    {
                        // managedContext?.delete(item)
                        return true
                    }
                }
            }
            
            // Save Changes
            //try managedContext?.save()
            
        } catch {
            // Error Handling
            // ...
        }
        
        return false
    } */
    
    /*class func saveRecentAddedFoods(  entityName: String, foodid: Int, foodname: String, foodFromTable: Int  )  -> Bool {
        //1
        CommonCoreDataFunctions.setupContext()
        
        
        if CommonCoreDataFunctions.isFoodExisted(entityName: entityName, foodId: foodid, foodFromTable: foodFromTable) == true
        {
            return false
        }
        
        
        //2
        /* let entity =  NSEntityDescription.entity( forEntityName: entityName ,
         in: CommonCoreDataFunctions.managedContext) */
        
        let food = NSEntityDescription.insertNewObject(forEntityName: entityName, into: managedContext ) as! CoreDataRecentlyUsedFoods
        // let food = CoreDataFavFoods(entity: entity!, insertInto: CommonCoreDataFunctions.managedContext )
        
        
        
        
        
        food.foodid = NSNumber(value: foodid )
        food.foodname = foodname
        food.foodfromtable = NSNumber(value: foodFromTable )
        
        
        //4
        do {
            try CommonCoreDataFunctions.managedContext.save()
            //5
            return true
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
        
        return false
    } */
    
    class func saveGeoLocation(  entityName: String, userId: String, longitude: Double, latitude: Double, address: String    )  -> Bool {
        //1
        CommonCoreDataFunctions.setupContext()
        
                let geoLocation = NSEntityDescription.insertNewObject(forEntityName: entityName, into: managedContext ) as! CoreDataGeoLocation
        // let food = CoreDataFavFoods(entity: entity!, insertInto: CommonCoreDataFunctions.managedContext )
        
        
    
        
        
        geoLocation.userid = NSString(string: userId )
        geoLocation.latitude = NSNumber(value:  latitude )
      
        geoLocation.longitude = NSNumber(value: longitude)
        geoLocation.adddate = NSDate()
        geoLocation.address =  NSString(string: address )
        
        
        //4
        do {
            try CommonCoreDataFunctions.managedContext.save()
            //5
            return true
        } catch let error as NSError  {
            print("Could not save \(error), \(error.userInfo)")
            return false
        }
        
        return false
    }
    
    
    
    /*class func fetchRecentUsedFoodsCoreData( entityName: String  ) -> [NSManagedObject]?
    {
        //1
        CommonCoreDataFunctions.setupContext()
        
        //2
        //  let fetchRequest = NSFetchRequest(entityName: "Person")
        let fetchRequest: NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: entityName )
        
        let sortDescriptor = NSSortDescriptor(key: "foodname", ascending: true )
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        //3
        do {
            let results =
                try CommonCoreDataFunctions.managedContext.fetch(fetchRequest)
            //var foods = results
            
            return results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
        return nil
    } */
    
    class func getStartDateTimeFromDate( date: Date ) -> NSDate
    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dtString = "\(dateFormatter.string(from: date)) 00:00"
        
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as! TimeZone // this line resolved me the issue of getting one day less than the selected date
        
        let dates:NSDate = dateFormatter.date(from: dtString)! as NSDate
        
        return dates
    }
    
    class func getEndDateTimeFromDate( date: Date ) -> NSDate
    {
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let dtString = "\(dateFormatter.string(from: date)) 23:59"
        
        
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        dateFormatter.timeZone = NSTimeZone(name: "GMT") as! TimeZone // this line resolved me the issue of getting one day less than the selected date
        
        let dates:NSDate = dateFormatter.date(from: dtString)! as NSDate
        
        return dates
    }
  
  
  class func fetchLatestRecordForCurrentDate(entityName: String, date: Date) -> [NSManagedObject]?
  {
    CommonCoreDataFunctions.setupContext()
    
    let fetchRequest: NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: entityName )
    fetchRequest.predicate = NSPredicate(format: "(adddate >= %@) AND (adddate <= %@)", CommonCoreDataFunctions.getStartDateTimeFromDate(date: date) , CommonCoreDataFunctions.getEndDateTimeFromDate(date: date) )
    
    let sortDescriptor = NSSortDescriptor(key: "adddate", ascending: false )
    let sortDescriptors = [sortDescriptor]
    fetchRequest.sortDescriptors = sortDescriptors
    
    fetchRequest.fetchLimit = 1
    
    //3
    do {
      let results =
        try CommonCoreDataFunctions.managedContext.fetch(fetchRequest)
      //var foods = results
      
      return results as! [NSManagedObject]
      
      
    } catch let error as NSError {
      print("Could not fetch \(error), \(error.userInfo)")
      return nil
    }
    return nil
    
  }
  
    class func fetchLocationsByDate( entityName: String, date: Date  ) -> [NSManagedObject]?
    {
        //1
        CommonCoreDataFunctions.setupContext()
        
        
        
        
        
        
        //2
        //  let fetchRequest = NSFetchRequest(entityName: "Person")
        let fetchRequest: NSFetchRequest<NSFetchRequestResult>  = NSFetchRequest(entityName: entityName )
        fetchRequest.predicate = NSPredicate(format: "(adddate >= %@) AND (adddate <= %@)", CommonCoreDataFunctions.getStartDateTimeFromDate(date: date) , CommonCoreDataFunctions.getEndDateTimeFromDate(date: date) )

        let sortDescriptor = NSSortDescriptor(key: "adddate", ascending: false )
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        
        //3
        do {
            let results =
                try CommonCoreDataFunctions.managedContext.fetch(fetchRequest)
            //var foods = results
            
            return results as! [NSManagedObject]
            
            
        } catch let error as NSError {
            print("Could not fetch \(error), \(error.userInfo)")
            return nil
        }
        return nil
    }
    
    
    class func deleteCoreDataRecordOldRecords(  entityName:String, olderThanDays: Int   )
    {
        CommonCoreDataFunctions.setupContext()
        
        let managedContext =  CommonCoreDataFunctions.managedContext
        
        let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: entityName)
        
        var dayComp = DateComponents()
        dayComp.day = olderThanDays * -1
        let date = Calendar.current.date(byAdding: dayComp, to: Date() )
        
        
        
        fetchRequest.predicate = NSPredicate(format: "(adddate <= %@)",  date as! NSDate )
        
        let deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
     
        do {
       try (UIApplication.shared.delegate as! AppDelegate).persistentContainer.persistentStoreCoordinator.execute(deleteRequest, with:  CommonCoreDataFunctions.getContext() )
        }
        catch let error as NSError
        {
            print(error)
        }
        
        
    }
    
    
}


//
//  CoreDataGeoLocation.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 20/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import Foundation
import CoreData

class CoreDataGeoLocation: NSManagedObject
{
    
    
    @NSManaged var userid: NSString!
     @NSManaged var address: NSString!
    @NSManaged var longitude: NSNumber!
    @NSManaged var latitude: NSNumber!
    @NSManaged var adddate: NSDate!
    
    
    
}

//
//  SettingsViewController.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 07/04/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource
{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrRowsData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.row == 0 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cellAccuracy" ) as? AccuracyTableViewCell
            {
                
                cell.label.text = "Location Accuracy"
                cell.slider.tag = indexPath.row
                cell.slider.value = arrRowsData[0] as? Float ?? 100.0
                cell.lblAccuracyValue.text = "\(Int(cell.slider.value)) Meters"
                cell.slider.addTarget(self, action: #selector(accuracySliderChanged(_:)), for: .valueChanged)
                return cell
            }
            
        }
        else if indexPath.row == 1 {
            if let cell = tableView.dequeueReusableCell(withIdentifier: "cellAccuracy" ) as? AccuracyTableViewCell
            {
                cell.label.text = "Location Time"
                cell.slider.tag = indexPath.row
                cell.slider.value = arrRowsData[1] as? Float ?? 60.0
                cell.lblAccuracyValue.text = "\(Int(cell.slider.value)) Mins."
                cell.slider.maximumValue = 60.0
                
                cell.slider.addTarget(self, action: #selector(timeSliderChanged(_:)), for: .valueChanged)
                return cell
            }
            
        }
        
        
        return UITableViewCell()
    }
    
    @objc func accuracySliderChanged(_ slider: UISlider )
    {
        if let cell = tableView.cellForRow(at: IndexPath(row: slider.tag, section: 0 ) ) as? AccuracyTableViewCell
        {
                cell.lblAccuracyValue.text = "\(Int(slider.value)) Meters"
        }
        arrRowsData[0] =  slider.value as AnyObject
        
        //print("slider changed \(slider.value)")
        LocalStorage.setValue(key: LocalStorageKeys.locationaccuracy , values: "\(slider.value)" )
        
        //LocalStorage.defaults.synchronize()
    }
    
    @objc func timeSliderChanged(_ slider: UISlider )
    {
        if let cell = tableView.cellForRow(at: IndexPath(row: slider.tag, section: 0 ) ) as? AccuracyTableViewCell
        {
            cell.lblAccuracyValue.text = "\(Int(slider.value)) Mins."
        }
        arrRowsData[1] =  slider.value as AnyObject
        
        //print("slider changed \(slider.value)")
        LocalStorage.setValue(key: LocalStorageKeys.locationtime , values: "\(slider.value)" )
        
        //LocalStorage.defaults.synchronize()
    }
}

class SettingsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var arrRowsData:[AnyObject] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.parent?.title = "Settings"

        // Do any additional setup after loading the view.
        tableView.delegate = self
        tableView.dataSource = self
    }

    override func viewWillAppear(_ animated: Bool) {
        
        var locationAccuracy: Float = 100.0
        var locationTime: Float = 60.0
        if LocalStorage.getValue(key: LocalStorageKeys.locationaccuracy ) != ""
        {
            let vl = LocalStorage.getValue(key: LocalStorageKeys.locationaccuracy )!
            locationAccuracy = Float( vl )  ?? 100.0
        }
        
        if LocalStorage.getValue(key: LocalStorageKeys.locationtime ) != ""
        {
            let vl = LocalStorage.getValue(key: LocalStorageKeys.locationtime )!
            locationTime = Float( vl )  ?? 60.0
        }
        
        arrRowsData.append( locationAccuracy as AnyObject )
        //arrRowsData.append( locationTime as AnyObject )
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    
    
}

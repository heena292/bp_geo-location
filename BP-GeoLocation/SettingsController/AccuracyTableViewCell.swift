//
//  AccuracyTableViewCell.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 07/04/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

import UIKit

class AccuracyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var lblAccuracyValue: UILabel!
    
    @IBOutlet weak var slider: UISlider!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

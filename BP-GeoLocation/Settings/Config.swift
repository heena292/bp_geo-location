//
//  Config.swift
//  BP-GeoLocation
//
//  Created by Hardeep Singh on 19/03/18.
//  Copyright © 2018 Business Pointers. All rights reserved.
//

//
//  config.swift
//  BP-Finannce
//
//  Created by Hardeep Singh on 11/07/17.
//  Copyright © 2017 Hardeep Singh. All rights reserved.
//

import Foundation
import UIKit

struct Colors
{
    
    public static var mainBlue =  UIColor(red: 0, green: 0.36, blue: 1, alpha: 1)
    
    public static var lightBlue = UIColor(red: 0, green: 0.48, blue: 1.0, alpha: 1.0)
    
    public static var extraLightBlue =  UIColor(red: 0.4, green: 0.8, blue: 1.0, alpha: 1 )
    
    public static var moreExtraLightBlue =  UIColor(red: 0.4, green: 0.8, blue: 1, alpha: 0.5 )
    
}

enum MsgTypes: Int {
    case success = 1, errors
}


